import re # regular expressions
from bs4 import BeautifulSoup
from selenium import webdriver
import numpy as np
import pandas as pd


class KrishaParser(object):
    def __init__(self, driver, city):
        self.driver = driver
        self.city = city
        self.cities = []
        self.names = []
        self.areas = []
        self.distincts = []
        self.address = []
        self.prices = []

    def parse(self):
        lastPage = self.paginationLastIndex()
        page = 1
        while page <= int(lastPage):
            self.apartament_page(page)
            page = page + 1
        data = {
            'city': self.cities,
            'name': self.names,
            'price': self.prices,
            'area': self.areas,
            'address': self.address,
            'distinct': self.distincts
        }
        df = pd.DataFrame(data)
        df.to_csv('krishakz.csv', encoding='utf-8-sig', index=False)
    
    def apartament_page(self, page):
        self.driver.get("https://krisha.kz/arenda/kvartiry/{0}/?das[rent.period]=2&page={1}".format(self.city, page))
        blocks = self.driver.find_elements_by_class_name('ddl_product')
        for block in blocks:
            title = block.find_element_by_class_name('a-card__title')
            price = block.find_element_by_class_name('a-card__price')
            area = block.find_element_by_class_name('a-card__subtitle')

            title = title.text
            area = area.text
        
            title_a = title.split(', ')
            area_a = area.split(', ')
            if(len(title_a) >= 2):
                self.names.append(title_a[0])
                self.areas.append(title_a[1])
            else:
                self.names.append(title_a[0])
                self.areas.append('null')

            if(len(area_a) >= 2):
                self.distincts.append(area_a[0])
                self.address.append(area_a[1])
            else:
                self.distincts.append('null')
                self.address.append(area_a[0])

            self.cities.append(self.city)
            self.prices.append(price.text)


    def paginationLastIndex(self):
        self.driver.get("https://krisha.kz/arenda/kvartiry/{0}/?das[rent.period]=2".format(self.city))
        numbers = self.driver.find_elements_by_class_name("paginator__btn")
        
        arr = []
        for num in numbers:
            if num.text.isnumeric():
                arr.append(num.text)

        arr = np.array(arr)
        return arr[-1]
    

def main():
    CHROME_DRIVER_PATH = r'C:\Users\алсер\Downloads\chromedriver_win32\chromedriver.exe'
    driver = webdriver.Chrome(CHROME_DRIVER_PATH)
    driver.get("https://krisha.kz")
    data = KrishaParser(driver, "almaty")
    data.parse()

if __name__ == "__main__":
    main()